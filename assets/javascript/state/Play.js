define(['module/Background','module/Player','module/Aliens','module/Bullets','module/Explosions','module/HUD'],function(Background,Player,Aliens,Bullets,Explosions,HUD){
    var _game = null,
        _nextState = null;
    
    var aliens = null;
    var aliens2 = null;
    
    //Playing State
    var _Play = {        
        create: function(){
                                   
            Background.create();
            
            HUD.createStat(0,100,3);
            
            //Setting up Player
            var playerConfiguration = {
                health: 100,
                lives: 3,
                score: 0,
                firingTime: 300,
                bulletSpeed: 500
            };
            
            Player.create(playerConfiguration);
            Player.setBulletGroup(Bullets.create(10,'bullet',100));
            Player.setExplosionGroup(Explosions.create(1,'kaboom'));

            function ShipsFactory() {}
            
            //Setting up Aliens
            var alienConfiguration = {
                rows:2,
                cols:10,
                scoreValue:10,
                firingTime: 100,
                bulletSpeed:200,
                health: 100,
                easing: Phaser.Easing.Sinusoidal.In
            };

            var alienConfiguration2 = {
                rows:3,
                cols:10,
                scoreValue:10,
                firingTime: 100,
                bulletSpeed:200,
                health: 100,
                easing: Phaser.Easing.Sinusoidal.Out
            };

            aliens = Aliens.create(alienConfiguration);
            aliens.setBulletGroup(Bullets.create(300,'enemyBullet',10));
            aliens.setExplosionGroup(Explosions.create(5,'kaboom'));
            Aliens.setPlayerShip(Player.getPlayerShip());

            aliens2 = Aliens.create(alienConfiguration2);
            aliens2.setBulletGroup(Bullets.create(3,'enemyBullet',10));
            aliens2.setExplosionGroup(Explosions.create(5,'kaboom'));
            Aliens.setPlayerShip(Player.getPlayerShip());

            // Player.setAliensAndAlienGroup(aliens);
            Player.setAliensAndAlienGroup(aliens2);
            
            //They start shoting, shooting is triggered by a time loop
            Player.startShooting();
            aliens.startShooting();
        },
        update: function(){
            Background.update();
            Player.update();

            //Setting up the collision handling
            aliens.createOverLap(Player.getBulletGroup());
            Player.createOverLap(aliens.getBulletGroup());

             aliens2.createOverLap(Player.getBulletGroup());
            Player.createOverLap(aliens2.getBulletGroup());
        }        
    }
    
    return{
        init: function(game,nextState){
            _game = game;
            _nextState = nextState;
        },
        getPlayState: function(){
            return(_Play);
        }
    }
})