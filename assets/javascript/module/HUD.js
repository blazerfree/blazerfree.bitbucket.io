define(function(){
    //Private variables
    var _game = null,
        _health = null,
        _healthText = null,
        _lives = null,
        _livesText = null,
        _score = null,
        _scoreText = null,
        _stateText = null;

    return{
        init: function(game){
            _game = game;
        },
        preload: function(){
            //_game.load.image('ship', 'assets/img/player.png');
        },
        createStat: function(score,health,lives){
            _score = score;
            _scoreText = _game.add.text(330, 10, "Очки: " + score, { fontSize: '34px', fill: '#fff' });
            _health = health;
            _healthText = _game.add.text(330, 50, "Здоровье: " + health, { fontSize: '34px', fill: '#fff' });
            _lives = lives;
            _livesText = _game.add.text(330, 90, "Жизни: " + lives, { fontSize: '34px', fill: '#fff' });
            
            //_stateText.visible = false;
        },
        updateHealthText: function(health){
            _healthText.text = "Здоровье: "+health;
        },
        updateLivesText: function(lives){
            _livesText.text = "Жизни: "+lives;
        },
        updateScoreText: function(score){
            _scoreText.text = "Очки: "+(_score+=score);
        },
        createTitle: function(title){
            _stateText = _game.add.text(_game.world.centerX,_game.world.centerY,
                                            title,{font: '44px Arial',fill: '#fff'})
            _stateText.anchor.setTo(0.5,0.5);
        }
    }
});