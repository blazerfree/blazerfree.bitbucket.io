define(function(){

    //Private variables
    var _game = null,
        _starfield = null;

    return{
        init: function(game,level,mode){
            _game = game;
        },
        preload: function(){
            _game.load.image('starfield', 'assets/css/starfield.jpeg');            
        },
        create: function(){
            _starfield = _game.add.tileSprite(0, 0,1000,1000, 'starfield');
        },
        update: function(){
            _starfield.tilePosition.y +=2;
        }

    }
});